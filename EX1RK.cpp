#include <iostream>
#include <string>
using namespace std;             
int main() {
	int size, fl = 1, k = 0;
	cin >> size;
	char c;
	cin >> c;
	if (size > 0) {
		int* a = new int[size];
		for (int i = 0; i < size; i++) {
			a[i] = 0;
		}
		while (c != '\n') {
			if (c == ' ') {
				k++;
			}
			else {
				a[k] = a[k] * 10 + c - 48;
			}
			cin.get(c);
		}
		if ((size > 0) && (k == size - 1)) {
			for (int i = 0; i < size / 2; i++) {
				swap(a[i], a[size - 1 - i]);
			}
			for (int i = 0; i < size; i++) {
				cout << a[i] << " ";
			}
		}
		else
			cout << "An error has occurred while reading input data";
	}
	cin.get();
	return(0);
}
